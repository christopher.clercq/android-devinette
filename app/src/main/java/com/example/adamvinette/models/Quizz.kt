package com.example.adamvinette.models

import java.io.Serializable

data class Quizz (var question:String, var answers:List<String>, var correct: Int) : Serializable
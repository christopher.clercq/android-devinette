package com.example.adamvinette.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.adamvinette.R;
import com.example.adamvinette.activities.ShowTextActivity;

public class Fragment1 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment1, container, false);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView myTextView = view.findViewById(R.id.titleTextView);
        final EditText editText = view.findViewById(R.id.editText);
        Button button = view.findViewById(R.id.buttonOk);
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String inputText = editText.getText().toString();
                        if (inputText.equals("toto")) {
                            Intent intent = new Intent(getActivity(), ShowTextActivity.class);
                            intent.putExtra("key", inputText);
                            startActivity(intent);
                        } else {
                            myTextView.setText(R.string.error_wrong_input);
                        }
                    }
                }
        );
    }

}

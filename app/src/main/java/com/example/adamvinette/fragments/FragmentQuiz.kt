package com.example.adamvinette.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.adamvinette.R
import com.example.adamvinette.interfaces.iQuizListener
import com.example.adamvinette.models.Quizz

class FragmentQuiz : Fragment(), View.OnClickListener {

    var quizz: Quizz? = null
    var button1: Button? = null
    var button2: Button? = null
    var button3: Button? = null
    var button4: Button? = null

    companion object {
        fun newInstance(quizz: Quizz): FragmentQuiz {
            val args = Bundle()
            args.putSerializable("key", quizz)
            val fragmentQuiz = FragmentQuiz()
            fragmentQuiz.arguments = args
            return fragmentQuiz
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (arguments != null) {
            quizz = arguments?.getSerializable("key") as Quizz
        }
        return inflater.inflate(R.layout.fragment_quizz, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val textView = view.findViewById<TextView>(R.id.question)
        textView.text = quizz?.question
        button1 = view.findViewById(R.id.button1)
        button2 = view.findViewById(R.id.button2)
        button3 = view.findViewById(R.id.button3)
        button4 = view.findViewById(R.id.button4)
        button1?.setOnClickListener(this)
        button2?.setOnClickListener(this)
        button3?.setOnClickListener(this)
        button4?.setOnClickListener(this)
        button1?.text = quizz?.answers?.get(0)
        button2?.text = quizz?.answers?.get(1)
        button3?.text = quizz?.answers?.get(2)
        button4?.text = quizz?.answers?.get(3)

    }

    override fun onClick(p0: View?) {
        if (p0?.id == button1?.id && quizz?.correct == 1) {
            correctAnswerClicked()
        } else if (p0?.id == button2?.id && quizz?.correct == 2) {
            correctAnswerClicked()
        } else if (p0?.id == button3?.id && quizz?.correct == 3) {
            correctAnswerClicked()
        } else if (p0?.id == button4?.id && quizz?.correct == 4) {
            correctAnswerClicked()
        } else {

        }
    }

    
    fun correctAnswerClicked() {
        if (activity is iQuizListener) {
            (activity as iQuizListener).onCorrectAnswerClicked()
        }
    }
}
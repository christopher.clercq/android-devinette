package com.example.adamvinette.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.adamvinette.R

class ShowTextActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_text_activity)
        val textView = findViewById<TextView>(R.id.second_textView)
        val textFromFirstActivity = intent.getStringExtra("key")
        textView.text = textFromFirstActivity

    }
}

package com.example.adamvinette.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.adamvinette.R
import com.example.adamvinette.fragments.FragmentQuiz
import com.example.adamvinette.fragments.SuccessFragment
import com.example.adamvinette.interfaces.iQuizListener
import com.example.adamvinette.models.Quizz

class MainActivity : AppCompatActivity(), iQuizListener {

    var listQuizz = ArrayList<Quizz>()
    var currentQuizz = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initQuizz()
        showQuizz()
    }

    override fun onCorrectAnswerClicked() {
        Toast.makeText(this,R.string.correct_answer,Toast.LENGTH_SHORT)
        currentQuizz++
        showQuizz()
    }

    private fun showQuizz(){
        val fragment : Fragment
        if(currentQuizz < listQuizz.size) {
            fragment = FragmentQuiz.newInstance(listQuizz[currentQuizz])

        } else {
            Toast.makeText(this,R.string.game_finish,Toast.LENGTH_SHORT)
            fragment = SuccessFragment()
        }
        val supportFragmentManager = supportFragmentManager
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, fragment)
        fragmentTransaction.commit()
    }

    fun initQuizz(){
        val question1 = "Qui meurt a la fin d'avenger 4 ?"
        val answers1 = ArrayList<String>()
        answers1.add("Jon Snow")
        answers1.add("Jean-claude Dus")
        answers1.add("Tony Stark")
        answers1.add("Iron Man")
        listQuizz.add(Quizz(question1, answers1, 3))
        val question2 = "Qu'est ce qui est jaune et qui attend ?"
        val answers2 = ArrayList<String>()
        answers2.add("Un poussin dans une file d'attente")
        answers2.add("Jonathan")
        answers2.add("Un chinois à la gare du nord")
        answers2.add("Le ricard")
        listQuizz.add(Quizz(question2, answers2, 2))

    }
}
package com.example.adamvinette.interfaces;

public interface iQuizListener {

    void onCorrectAnswerClicked();

}